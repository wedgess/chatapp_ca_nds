# Chat App - NDS CA #
---------------------------------------
* **Version:** *0.0.1*

## Summary ##

A chat application using Node.js and web sockets(Socket.io) for the backend as part of a Networking & Distributed Systems CA project. The front end consists of HTML, CSS, JavaScript and Socket.io. The front end is built on top of the [materializecss](http://www.materializecss.com) framework.

## Features ##

* Create Rooms
* Join/leave rooms
* Upload avatar
* Chrome notifications when not on the chat rooms chrome tab
* See when other users are typing

## How to run the project ##
* Must have **[Node.js](https://nodejs.org/en/download/) installed**.
* From the **root** directory run `node app.js` in a terminal/command prompt

## Project Structure ##
**Directory**
*Files*

- **root**
    + **node_modules**
        * Node.js* dependencies
    + **public** - contains JS and CSS files for front end
        * chat.html* - the landing page which contains the login and chat markup
        * chat.js* - the front end logic and communicates with app.js using socket.io
        * User.js* - contains the user object code
        * Message.js* - contains the message object code
        * Room.js* - contains the room object code
    + *app.js*
        * Contain the routes for socket.io and main backend logic
    + *package.json*
        * Information of the project and its dependencies

# Images #
----------------------------------------------
### Login ###
![Login Screen](http://i.imgur.com/eKYCtLe.png)

### Chat Room ###
![Chat Room Screen](http://i.imgur.com/fdxBkEe.png)

### Create Rooms ###
![Chat Room Screen](http://i.imgur.com/kryUs1H.png)

### Notifications/User Typing ###
![Notifications and user typing status](http://i.imgur.com/mluvYa5.png)