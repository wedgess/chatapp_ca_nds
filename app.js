/**
 * Look here for types of broadcasts which will be needed in future
 * http://stackoverflow.com/a/38933590/3748532
 * 
 * @type Module express|Module express
 */

var express = require('express');
var app = express(); // setup express
var fs = require('fs');
var server = require('http').createServer(app); // creates http server using app(express)
var io = require('socket.io')(server); // use socket.io

// file is included here:
eval(fs.readFileSync('./public/js/User.js') + '');
eval(fs.readFileSync('./public/js/Room.js') + '');

// stores all connected user objects
var users = [];
// stores all created room objects
var rooms = [new Room('General'), new Room('Networking & Dist')];

// expose node modules to web so client can use the modules
app.use(express.static(__dirname + '/node_modules'));
app.use(express.static(__dirname + '/public'));

// tells app to deal with GET request, when on root do...
app.get('/', function (request, response) {
    console.log("GET request for /");
    // send back the reponse which will be the index file
    response.sendFile(__dirname + '/public/chat.html');

});

// when some oneconnects to my socket
io.on('connection', function (client) {
    console.log('Client: ' + client.id + ' connected to socket..');

    // used to validate a username, and if valid stores user in the 'users' array
    client.on('user-validate', function (name, avatar) {
        // check if username is available
        if (usernameAvailable(name)) {
            // create new user
            var user = new User(client.id, name, avatar);
            sendUserAvailability(true, user);
            // set the username in client object
            client.username = user.name;
            // store user in users array
            users.push(user);
            // send the new user to all clients so user can be added to list of users
            client.room = rooms[0].name;
            rooms[0].addMember(user);
            client.join(client.room);
        } else {
            // send back username not availbale to client
            sendUserAvailability(false);
        }
    });

    // used to let all client know when a user is typing
    client.on('user-typing', function (user) {
        io.to(client.room).emit('user-typing', user);
    });

    // listen to when a client is NOT typing
    client.on('user-not-typing', function (user) {
        io.to(client.room).emit('user-not-typing', user);
    });

    client.on('room-refresh', function () {
        // send update to room that user has joined
        sendUpdateMessageToRoom(true);
        var roomObj = findRoomByName(client.room);
        if (roomObj) {
            // send updated list of room members to client
            sendUpdateRoomMembers(roomObj);
        }
    });

    client.on('room-change', function (newRoom) {
        console.log("Received room change for newRoom: " + newRoom);
        // get user from array of users
        var user = findUserById(client.id);
        // get the room from array of rooms
        var oldRoomObj = findRoomByName(client.room);
        var newRoomObj = findRoomByName(newRoom);
        if (user && oldRoomObj && newRoomObj) {
            console.log("Old room obj: " + oldRoomObj.name);
            // remove member from the old room
            oldRoomObj.removeMember(client.id);
            // send the update to the room before leaving
            sendRoomUpdate(oldRoomObj, false);
            client.leave(client.room);

            console.log("New room obj: " + newRoomObj.name);
            client.room = newRoom;
            client.join(newRoom);
            // set clients room and join the chat
            newRoomObj.addMember(user);
            sendRoomUpdate(newRoomObj, true);
        }
    });

    client.on('room-create', function (newRoom) {
        console.log("Received room change for newRoom: " + newRoom);
        // get user from array of users
        var user = findUserById(client.id);
        // get the room from array of rooms
        var oldRoomObj = findRoomByName(client.room);
        var newRoomObj = new Room(newRoom);
        rooms.push(newRoomObj);
        if (user && oldRoomObj && newRoomObj) {
            console.log("Old room obj: " + oldRoomObj.name);
            // remove member from the old room
            oldRoomObj.removeMember(client.id);
            // send the update to the room before leaving
            sendUpdateMessageToRoom(false);
            sendUpdateRoomMembers(oldRoomObj);
            client.leave(client.room);

            console.log("New room obj: " + newRoomObj.name);
            client.room = newRoom;
            client.join(newRoom);
            client.emit('message-clear');
            // set clients room and join the chat
            newRoomObj.addMember(user);
            sendUpdateRoomMembers(newRoomObj);
            io.emit('room-list', rooms);
        }
    });

    // when room list is requested, return rooms array
    client.on('get-room-list', function () {
        client.emit('room-list', rooms);
    });

    // listen to a message being sent
    client.on('message-outgoing', function (msg) {
        client.broadcast.to(client.room).emit('message-incoming', msg);
    });

    // listen to when a client disconnect, and remove them from room and update that room
    client.on('disconnect', function () {
        console.log('Got disconnect!');
        for (var i in users) {
            if (users[i].id === client.id) {
                var roomObj = findRoomByName(client.room);
                if (roomObj) {
                    roomObj.removeMember(client.id);
                    // remove user from users array and send update to room
                    users.splice(i, 1);
                    sendRoomUpdate(roomObj, false);
                } else {
                    console.log(client.username + ' has disconnected but their previous room is unknown!');
                }
                return;
            }
        }
    });

    /***************** Client Helpers *******************/

    function sendUpdateRoomMembers(roomObject) {
        // sends message to room, object contains all rooms members so they can be updated
        io.to(client.room).emit('room-update-members',
                {
                    roomName: roomObject.name,
                    members: roomObject.members
                }
        );
    }

    function sendUpdateMessageToRoom(joined) {
        // send update message to the room for when a user leaves or joins room
        client.broadcast.to(client.room).emit('room-update-msg',
                {
                    username: client.username,
                    hasJoined: joined
                }
        );
    }

    function sendRoomUpdate(roomObj, joined) {
        // update called for changing room
        sendUpdateRoomMembers(roomObj);
        sendUpdateMessageToRoom(joined);
        client.broadcast.emit('room-member-count-update',
                {
                    roomName: roomObj.name,
                    members: roomObj.members
                }
        );
    }

    // emits to client if the username is available
    function sendUserAvailability(available, user) {
        client.emit('user-availability',
                {
                    available: available,
                    user: user
                }
        );
    }
});

// check users array to see if username is available
function usernameAvailable(name) {
    for (var index in users) {
        if (users[index].name === name) {
            return false;
        }
    }
    return true;
}

function findRoomByName(name) {
    for (var index in rooms) {
        if (rooms[index].name === name) {
            return rooms[index];
        }
    }
    return null;
}

function findUserById(id) {
    for (var index in users) {
        if (users[index].id === id) {
            return users[index];
        }
    }
    return null;
}

server.listen(3000); // listen on port 3000