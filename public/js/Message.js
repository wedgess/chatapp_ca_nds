var viewTemplate = '<div class="row row-bubble">\
                        {{avatar-img}}\
                        <div class="{{bubble-type}}">\
                            <div style="color: {{nameColor}}; font-weight: bold; margin-bottom: 2px;">\
                                    {{sender}}\
                            </div>\
                            <div class="talktext">\
                                {{message}}   \
                            </div>\
                            <div class="msg-timestamp-view">\
                                <i class="material-icons tiny msg-timestamp-icon">query_builder</i>\
                                    <span class="msg-timestamp-text">{{timestamp}}</span>\
                            </div>\
                        </div>\
                    </div>'

"use strict";

function Message(msg, senderName, avatar) {
    this.msg = msg;
    if (senderName === undefined) {
        this.senderName = '';
    } else {
        this.senderName = senderName;
    }
    if (avatar === undefined) {
        this.avatar = '';
    } else {
        this.avatar = avatar;
    }
}

Message.prototype.getView = function (isOutgoingMessage) {

    function stringToColour(str) {
        var hash = 0;
        for (var i = 0; i < str.length; i++) {
            hash = str.charCodeAt(i) + ((hash << 5) - hash);
        }
        var colour = '#';
        for (var i = 0; i < 3; i++) {
            var value = (hash >> (i * 8)) & 0xFF;
            colour += ('00' + value.toString(16)).substr(-2);
        }
        return colour;
    }

    function timeFormat(date) {

        function formatTwoDigits(num) {
            return num < 10 ? '0' + num : num;
        }

        hours = formatTwoDigits(date.getHours());
        minutes = formatTwoDigits(date.getMinutes());
        seconds = formatTwoDigits(date.getSeconds());
        return hours + ":" + minutes + ":" + seconds;
    }

    var template = document.createElement('template');
    var bubbleClass = '';
    var senderName = '';
    var avatarTag = '<img src="{{avatar-img}}" alt="" class="circle msg-avatar">';
    // this changes the type of bubble - outgoing messages don't have avatar or sender name and different color bubble
    if (isOutgoingMessage) {
        bubbleClass = 'talk-bubble-outgoing tri-left right-top';
        avatarImg = '';
        senderName = '';
    } else {
        senderName = this.senderName;
        bubbleClass = 'talk-bubble-incoming tri-right left-top';
        avatarImg = avatarTag.replace(/{{avatar-img}}/g, this.avatar);
    }
    // replace name placeholder inside the template with this users name
    var msgTemplate = viewTemplate.replace(/{{message}}/g, this.msg)
            .replace(/{{sender}}/g, senderName)
            .replace(/{{avatar-img}}/g, avatarImg)
            .replace(/{{bubble-type}}/g, bubbleClass)
            .replace(/{{timestamp}}/g, timeFormat(new Date()))
            .replace(/{{nameColor}}/g, stringToColour(this.senderName));
    // set the innerHTML on the player list
    template.innerHTML = msgTemplate;

    return template.content.firstChild;

}

Message.prototype.toJsonString = function () {
    return JSON.stringify({'message': this.msg, 'senderName': this.senderName, 'avatar': this.avatar});
}