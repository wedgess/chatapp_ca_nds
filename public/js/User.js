var userTemplate = '<li class="card collection-item avatar" style="margin: 0 auto; vertical-align: center;">\
      <img src="{{image-data}}" alt="" class="circle msg-avatar">\
      <span class="title">{{name}}</span>\
      <p id="{{name}}__status" style="display:none; font-size: 12px;">\
      </p>\
    </li>';

"use strict";

function User(id, name, avatar) {
    this.id = id;
    this.name = name;
    if (avatar === undefined) {
        this.avatar = '';
    } else {
        this.avatar = avatar;
    }
}

User.prototype.getView = function (callback) {

    function createDMButton(name) {
        var anchor = document.createElement("a");
        anchor.className = "secondary-content waves-effect waves-teal";
        var anchorIcon = document.createElement("i");
        anchorIcon.className = "material-icons";
        anchorIcon.innerHTML = "chat";
        anchor.appendChild(anchorIcon);
        // add event listener for this button
        anchor.addEventListener('click', function () {
            console.log(name + " clicked!");
            callback(name);
        });

        return anchor;
    }

    var template = document.createElement('template');
    // replace name placeholder inside the template with this users name
    var templateText = userTemplate.replace(/{{name}}/g, this.name)
            .replace(/{{image-data}}/, this.avatar);
    // set the innerHTML on the player list
    template.innerHTML = templateText;

    var node = template.content.firstChild;
    // append send DM button user
    node.appendChild(createDMButton(this.name));

    return node;
};

User.prototype.toJsonString = function () {
    return JSON.stringify({'id': this.id, 'name': this.name, 'avatar': this.avatar});
};


