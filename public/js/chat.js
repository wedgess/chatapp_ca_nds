window.onload = function () {
    "use strict";

    var socket = io.connect();
    var msgInput = document.getElementById('input-container__msg-input');
    var usernameInput = document.getElementById('login-form__username');
    var sendMsgBtn = document.getElementById('input-container__send');
    var messagesContainer = document.getElementById('messages-container');
    var usersContainer = document.getElementById('users-container');
    var roomsContainer = document.getElementById('rooms-container');
    var fileUploadInput = document.getElementById('file-upload__input');
    var loginBtn = document.getElementById('login-form__submit');
    var loginAvatarPreview = document.getElementById('login-card__avatar');
    var loginErrorField = document.getElementById('login-form__error');
    var uploadProgressContainer = document.getElementById('upload-progress__container');
    var user = {};
    var showNotification = false;

    /* ***************** Socket IO Listeners ***************** */

    // listen to if the username is available or not
    socket.on('user-availability', function (userData) {
        console.log("Username is available:" + userData.available);
        if (userData.available) {
            user = userData.user;
            // get the welcome title, to dispplay current users name
            document.getElementById('room-title-container__user').innerHTML = 'Welcome ' + userData.user.name;
            // if error field is not hidden, then hide it
            if (loginErrorField.style.visibility !== 'hidden') {
                loginErrorField.style.visibility = 'hidden';
            }
            // get list of all rooms
            socket.emit('get-room-list');
            socket.emit('room-refresh');
            // hide login section and show chat section
            var loginSection = document.getElementById('login-content');
            var chatSection = document.getElementById('chat-content');
            chatSection.style.display = 'block';
            loginSection.style.display = 'none';
            // initialize materializecss tabs and modal
            initMaterializecss();
        } else {
            // Error - username already exist
            loginErrorField.innerHTML = 'Username already exists!';
            loginErrorField.style.visibility = 'visible';
        }
    });

    socket.on('room-members-list', function (members) {
        fillMembersContainer(members);
    });

    // listen to room updates, if user has left or joined room.
    socket.on('room-update-msg', function (userData) {
        var msgRow = document.createElement('p');
        // set message based on if user has joined or left chat, also add class with color
        var message;
        if (userData.hasJoined) {
            message = userData.username + " has joined this room";
            msgRow.className = "joined-text";
        } else {
            message = userData.username + " has left this room";
            msgRow.className = "leave-text";
        }
        // set the message text and add to message container
        msgRow.innerHTML = message;
        messagesContainer.appendChild(msgRow);
        // scroll to bottom of container
        messagesContainer.scrollTop = messagesContainer.scrollHeight;
    });

    // listen for when list of all users is sent
    socket.on('room-update-members', function (roomData) {
        // get members array from the roomData sent from server
        var members = roomData.members;
        // fill the rooms members container
        fillMembersContainer(members);
        // set the rooms member count in UI
        setRoomsMemberCount(roomData);
    });

    socket.on('room-member-count-update', function (roomData) {
        // set the rooms member count in UI
        setRoomsMemberCount(roomData);
    });

    // listen for when list of all rooms is sent
    socket.on('room-list', function (rooms) {

        // creates row for -- create new chat button
        function appendNewChatButton() {
            var template = document.createElement('template');
            template.innerHTML = '<div id="add-card" class="card no-btm-margin circle">\
                                    <div class="add-btn-container">\
                                        <a class="waves-effect waves-light btn-floating add-btn" href="#modal1">\
                                            <i class="material-icons">add</i>\
                                        </a>\
                                    </div>\
                                    <span class="btn-title">New Chat</span>\
                                </div>'

            return template.content.firstChild;
        }

        while (roomsContainer.firstChild) {
            roomsContainer.removeChild(roomsContainer.firstChild);
        }
        // add the new chat button as first item of container
        roomsContainer.appendChild(appendNewChatButton());
        // for each room in rooms array, append the view to rooms container
        for (var i in rooms) {
            var room = new Room(rooms[i].name, rooms[i].members);
            roomsContainer.appendChild(room.getView(joinChatButtonCallback));
        }
    });
    
    // this is sent from server when the user has created a new room
    socket.on('message-clear', function() {
        clearMessageContainer();
    });

    // listen for when a new message is received
    socket.on('message-incoming', function (msg) {
        console.log("Received message: " + msg);
        var message = parseMessage(msg);
        // append message to the message container
        messagesContainer.appendChild(message.getView(false));
        // scroll to bottom
        messagesContainer.scrollTop = messagesContainer.scrollHeight;
        // show chrome notification if tab is not active
        if (showNotification) {
            notifyMe(message);
        }
    });

    socket.on('user-typing', function (user) {
        var userStatus = document.getElementById(user.name + '__status');
        if (userStatus) {
            // set the style and visibility of typing status
            userStatus.style.display = "block";
            userStatus.style.color = "#4CAF50";
            userStatus.innerHTML = "Typing...";
        }
    });

    socket.on('user-not-typing', function (user) {
        //console.log("Received not typing");
        var userStatus = document.getElementById(user.name + '__status');
        if (userStatus) {
            // hide typing status view
            userStatus.style.display = "none";
        }
    });

    /* ***************** Socket Utils ***************** */

    function joinChatButtonCallback(newRoomName) {
        // send new room to server
        socket.emit('room-change', newRoomName);
        setRoomTitle(newRoomName);
        // remove all messages, otherwise messages from previous room will still show
        clearMessageContainer();
    }
    
    function clearMessageContainer() {
        // remove all messages from the message container
         while (messagesContainer.firstChild) {
            messagesContainer.removeChild(messagesContainer.firstChild);
        }
    }

    function setRoomTitle(title) {
        var roomTitleEl = document.getElementById('room-title-container__title');
        if (roomTitleEl) {
            roomTitleEl.innerHTML = title + ' Chat';
        } else {
            console.log("Error: Room title element not found");
        }
    }

    function setRoomsMemberCount(roomObj) {
        console.log(roomObj);
        // get the rooms member count span element
        var roomCountEl = document.getElementById(roomObj.roomName + '__member-count');
        if (roomCountEl) {
            // set number of room members
            roomCountEl.innerHTML = roomObj.members.length;
        } else {
            console.log("Error: Element with ID: " + roomObj.roomName + '__member-count' + " --> not found");
        }
    }

    // used to add member views to members container
    function fillMembersContainer(members) {
        // remove each user from the users container
        while (usersContainer.firstChild) {
            usersContainer.removeChild(usersContainer.firstChild);
        }
        // readd each member to users container
        for (var i in members) {
            var member = new User(members[i].id, members[i].name, members[i].avatar);
            // apend user row with callback for when a users DM icon is clicked
            usersContainer.appendChild(member.getView());
            // if the user added is the current user
            if (member.id === user.id) {
                var thisUserCard = usersContainer.lastChild;
                // remove the send message button
                thisUserCard.removeChild(thisUserCard.lastChild);
                // add (me) to show them who they are in the list of users
                thisUserCard.getElementsByTagName('span')[0].innerHTML += ' <span style="color: navy;">(me)</span>';
            }
        }
    }

    /* ***************** Event Listeners ***************** */

    // listen to when user leaves tab, if they have showNotification otherwise don't show it
    document.addEventListener('visibilitychange', function () {
        showNotification = document.visibilityState === 'hidden';
    })

    // login button clicked
    loginBtn.addEventListener('click', function (event) {
        event.preventDefault();
        // make sure there is valid input before checking if a user exists with same name
        if (usernameInput.value !== '') {
            // if error field is not hidden, then hide it
            if (loginErrorField.style.visibility !== 'hidden') {
                loginErrorField.style.visibility = 'hidden';
            }
            // send username to server for validating, and send the avatar image data
            socket.emit('user-validate', usernameInput.value, loginAvatarPreview.getAttribute('src'));
        } else {
            // user name is blank, show error message
            loginErrorField.innerHTML = "Username cannot be blank!";
            loginErrorField.style.visibility = 'visible';
        }
    });

    // lsiten to if the user is typing
    usernameInput.addEventListener('input', function (event) {
        socket.emit("user-typing", user);
        // if error field is not hidden, then hide it
        if (loginErrorField.style.visibility !== 'hidden') {
            loginErrorField.style.visibility = 'hidden';
        }
        if (uploadProgressContainer.style.display === 'block') {
            uploadProgressContainer.style.display = 'none';
            loginBtn.style.display = 'block';
        }
    });

    // listen to when message input looses focus to send user not typing msg
    msgInput.addEventListener('blur', function (event) {
        //console.log("Lost focus");
        socket.emit("user-not-typing", user);
    });

    // lsiten to if the user is typing
    msgInput.addEventListener('input', function (event) {
        socket.emit("user-typing", user);
    });

    // send message when user hits enter
    msgInput.addEventListener("keyup", function (event) {
        event.preventDefault();
        // if enter key is pressed
        if (event.keyCode === 13) {
            // ssubmit message
            sendMsgBtn.click();
        }
    });

    // when input type=file is clicked show the upload progress bar (only way)
    fileUploadInput.addEventListener('click', function (event) {
        uploadProgressContainer.style.display = 'block';
        loginBtn.style.display = 'none';
    });

    // listen to when a file has been selected
    fileUploadInput.addEventListener('change', function (event) {
        var value = fileUploadInput.value;
        var fileName = value.substring(value.lastIndexOf("\\") + 1);
        console.log("Received file: " + fileName);
        //document.getElementById("file-upload-submit").click();
        var file = document.querySelector('input[type=file]').files[0];
        var reader = new FileReader();

        // listen to when readers data is loaded
        reader.addEventListener("load", function () {
            var imgData = reader.result;
            loginAvatarPreview.setAttribute('src', imgData);
            // hide PB as image is set
            uploadProgressContainer.style.display = 'none';
            loginBtn.style.display = 'block';
        }, false);

        if (file) {
            reader.readAsDataURL(file);
        }
    });

    // click listner for sens message button
    sendMsgBtn.addEventListener('click', function () {
        var msg = msgInput.value;
        // make sure message is not empty
        if (msg !== '') {
            var message = new Message(msg, user.name, user.avatar);
            // add message view to message container
            messagesContainer.appendChild(message.getView(true));
            // clear the current msg input
            msgInput.value = '';
            // scroll to bottom
            messagesContainer.scrollTop = messagesContainer.scrollHeight;
            socket.emit('message-outgoing', message.toJsonString());
            socket.emit("user-not-typing", user);
        }
    });

    // parse from JSON to message object
    function parseMessage(msg) {
        var msgJson = JSON.parse(msg);
        return new Message(msgJson.message, msgJson.senderName, msgJson.avatar);
    }

    // request permission for chrome notifications on page load
    document.addEventListener('DOMContentLoaded', function () {
        if (!Notification) {
            alert('Desktop notifications not available in your browser. Try Chromium.');
            return;
        }
        if (Notification.permission !== "granted") {
            Notification.requestPermission();
        }
    });

    // show chrome notification
    function notifyMe(message) {
        if (Notification.permission !== "granted") {
            Notification.requestPermission();
        } else {
            // notifications data
            var notification = new Notification('New Message', {
                icon: '../images/ic_launcher.png',
                body: message.senderName + ': ' + message.msg
            });

            notification.onclick = function () {
                // got to tab
                window.focus();
                this.cancel();
            };
        }
    }

    // attach event listener to create new room button
    document.getElementById('create-room-form__submit').addEventListener('click', function (event) {
        var newRoomNameEl = document.getElementById('create-room-form__roomname');
        if (newRoomNameEl) {
            // get the new room name from input and if not empty create room on server
            var roomName = newRoomNameEl.value;
            if (roomName !== '') {
                socket.emit('room-create', roomName);
                setRoomTitle(roomName);
                newRoomNameEl.value = '';
            } else {
                // room name is empty don;t submit
                event.preventDefault();
            }
        }
    });

    /******* Materialize CSS initializer *******/
    /*
     * This jQuery needs to be here to load the materializecss tabs.
     * If used in chat.html the tabs aren't visible so this doesn't work.
     * This selects the default active tab and shows the indicator.
     */
    function initMaterializecss() {
        $(document).ready(function () {
            $('ul.tabs').tabs();
            // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
            $('.modal').modal();
        });
    }
};