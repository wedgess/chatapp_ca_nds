var roomTemplate = '<li class="card collection-item" style="margin: 0 auto; vertical-align: center;">\
      <span class="title">{{name}}</span>\
      </br>\
      <span id="{{name}}__member-count">\
        {{members_count}}\
      </span>\
      members\
    </li>';

"use strict";

function Room(name, members) {
    this.name = name;
    if (members === undefined) {
        this.members = [];
    } else {
        this.members = members;
    }
}

Room.prototype.getView = function (callback) {

    function createButton(name, callback) {
        var anchor = document.createElement("a");
        anchor.className = "secondary-content waves-effect";
        var anchorIcon = document.createElement("i");
        anchorIcon.className = "material-icons";
        anchorIcon.innerHTML = "launch";
        anchor.appendChild(anchorIcon);
        // add event listener for this button
        anchor.addEventListener('click', function () {
            console.log("Join " + name + " clicked!");
            callback(name);
        });

        return anchor;
    }

    var template = document.createElement('template');
    // replace name placeholder inside the template with this users name
    var templateText = roomTemplate.replace(/{{name}}/g, this.name)
            .replace(/{{members_count}}/g, this.members.length);
    // set the innerHTML on the player list
    template.innerHTML = templateText;

    var node = template.content.firstChild;
    // append send DM button user
    node.appendChild(createButton(this.name, callback));

    return node;
}

Room.prototype.addMember = function (member) {
    this.members.push(member);
}

Room.prototype.removeMember = function (memberId) {
    for (var index in this.members) {
        if (this.members[index].id === memberId) {
            this.members.splice(index, 1);
        }
    }
}